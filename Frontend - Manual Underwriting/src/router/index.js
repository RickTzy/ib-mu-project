import Vue from 'vue'
import VueRouter from 'vue-router'
import ProfileView from '../views/ProfileView.vue'
import AdminView from '../views/AdminView.vue'
import ValidatorView from '../views/ValidatorView.vue'
import VerifikatorView from '../views/VerifikatorView.vue'
import DetailsView from '../views/DetailsView.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'profile',
    component: ProfileView
  },
  {
    path: '/admin',
    name: 'admin',
    component: AdminView
  },
  {
    path: '/verifikator',
    name: 'verifikator',
    component: VerifikatorView
  },
  {
    path: '/validator',
    name: 'validator',
    component: ValidatorView
  },
  {
    path: '/detailsView',
    name: 'detailsView',
    component: DetailsView
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
