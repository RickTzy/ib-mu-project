package com.example.internetbankingbackend.service;

import com.example.internetbankingbackend.jwt.TokenService;
import com.example.internetbankingbackend.model.Account;
import com.example.internetbankingbackend.model.AppUser;
import com.example.internetbankingbackend.repository.AccountRepository;
import com.example.internetbankingbackend.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class CustomerService {

    private PasswordEncoder passwordEncoder;

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private RedisService redisService;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TokenService tokenService;

    public CustomerService(AccountRepository accountRepository, RedisService redisService, AppUserRepository appUserRepository){
        this.accountRepository = accountRepository;
        this.redisService = redisService;
        this.appUserRepository = appUserRepository;
    }

    public ResponseEntity<Map<String, Object>> login(AppUser appUser, HttpServletResponse servletResponse) {
        Map<String, Object> response = new HashMap<>();
        passwordEncoder = new BCryptPasswordEncoder();

        Optional<AppUser> user = appUserRepository.findOneByUsernameAndStatus(appUser.getUsername(), "Active");
        if (user.isPresent()) {
            // logic bandingin password appuser dengan user
            AppUser existingUser = user.get();
            if (passwordEncoder.matches(appUser.getPassword(), existingUser.getPassword())) {
                String token = tokenService.generateToken(appUser.getUsername());
                // Cookie not found
                Cookie cookie = new Cookie("Token", token);
                cookie.setMaxAge(60);
                cookie.setHttpOnly(true);
                servletResponse.addCookie(cookie);

                existingUser.setLoginFailed(0);
                existingUser.setLogin(true);

                appUserRepository.save(existingUser);
                List<Account> accounts = accountRepository.findALlByCustomerId(existingUser.getUserId());
                redisService.setValue(appUser.getUsername(), accounts.toString());

                response.put("message", "You are successfully logged in!");
                response.put("token", token);
                return ResponseEntity.ok(response);

            } else {
                // kalau salah user diupdate login_failed +1
                if (existingUser.getLoginFailed() == 3) {
                    existingUser.setStatus("Inactive");
                    appUserRepository.save(existingUser);
                    response.put("message", "Your id is blocked");
                    return new ResponseEntity<>(response, HttpStatus.LOCKED);
                }
                existingUser.setLoginFailed(existingUser.getLoginFailed() + 1);
                appUserRepository.save(existingUser);
                response.put("message", "Sorry, the username or the password error!");
                return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
            }
        }
        response.put("message", "Sorry, the username is not registered or inactive");
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<Map<String, Object>> logout(AppUser appUser) {
        Map<String, Object> response = new HashMap<>();

        Optional<AppUser> user = appUserRepository.findOneByUsernameAndStatus(appUser.getUsername(), "active");
        AppUser existingUser = user.get();

        existingUser.setLogin(false);
        appUserRepository.save(existingUser);

        redisService.remove(appUser.getUsername());
        response.put("message", "You are successfully logged out!");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
