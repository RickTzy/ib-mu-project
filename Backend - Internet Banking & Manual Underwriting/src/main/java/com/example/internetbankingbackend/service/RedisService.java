package com.example.internetbankingbackend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.List;
import java.util.Set;

@Service
public class RedisService {

    @Autowired
    private RedisTemplate<String ,String> redisTemplate;

    public Object getValue(final String key) { return redisTemplate.opsForValue().get(key);}

    public void setValue(final String key , final String value){
        ValueOperations<String, String> operations = redisTemplate.opsForValue();
        operations.set(key, value);
    }

    public void remove(String key){redisTemplate.delete(key);}

    public void remove(List<String> keys){redisTemplate.delete(keys);}

    public Set<String> getAllKeys(){ return redisTemplate.keys("*");}

    public Set<String> getKeysByName(String key){ return redisTemplate.keys(key);}
}
