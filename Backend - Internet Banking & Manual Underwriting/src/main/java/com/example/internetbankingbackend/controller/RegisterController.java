package com.example.internetbankingbackend.controller;

import com.example.internetbankingbackend.exception.ResourceNotFoundException;
import com.example.internetbankingbackend.model.Account;
import com.example.internetbankingbackend.model.AppUser;
import com.example.internetbankingbackend.model.Customer;
import com.example.internetbankingbackend.repository.AccountRepository;
import com.example.internetbankingbackend.repository.AppUserRepository;
import com.example.internetbankingbackend.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.ResourceAccessException;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping
public class RegisterController {

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AppUserRepository appUserRepository;

    // Get All Customers Data
    @GetMapping("/customers")
    public List<Customer> getAllCustomers(){
        return customerRepository.findAll();
    }

    // Get All Accounts Data
    @GetMapping("/accounts")
    public List<Account> getAllAccounts() { return accountRepository.findAll(); }

    // Get All AppUsers Data
    @GetMapping("/appUsers")
    public List<AppUser> getAllAppUsers() { return appUserRepository.findAll(); }

    // Create Customer
    @PostMapping("/customer/post")
    private Customer createCustomer(@RequestBody Customer customer){
        return customerRepository.save(customer);
    }

    // Create Customers
    @PostMapping("/customers/post")
    private ResponseEntity<List<Customer>> createCustomers(@RequestBody List<Customer> customers){
        customerRepository.saveAll(customers);
        return ResponseEntity.ok(customers);
    }

    // Create Account
    @PostMapping("/account/post")
    private Account createAccount(@RequestBody Account account){
        return accountRepository.save(account);
    }

    // Create Accounts
    @PostMapping("/accounts/post")
    private ResponseEntity<List<Account>> createAccounts(@RequestBody List<Account> accounts){
        accountRepository.saveAll(accounts);
        return ResponseEntity.ok(accounts);
    }

    // Create AppUser
    @PostMapping("/appUser/post")
    private AppUser createAppUser(@RequestBody AppUser appUser){
        appUser.setPassword(passwordEncoder.encode(appUser.getPassword()));
        return appUserRepository.save(appUser);
    }

    // Create AppUsers
    @PostMapping("/appUsers/post")
    private ResponseEntity<List<AppUser>> createAppUsers(@RequestBody List<AppUser> appUsers){
        appUserRepository.saveAll(appUsers);
        return ResponseEntity.ok(appUsers);
    }

    // Register Page
    // Get Customer By pan
    @GetMapping("/customer/pan/{pan}")
    public ResponseEntity<Customer> getCustomerByPan(@PathVariable String pan){
        return new ResponseEntity<Customer>(customerRepository.findByPan(pan), HttpStatus.OK);
    }

    // Get Customer By customerId and pin
    @GetMapping("/customer/{id}/pin/{pin}")
    public ResponseEntity<Customer> getCustomerByCustomerIdAndPin(@PathVariable Integer id, @PathVariable Integer pin){
        return new ResponseEntity<Customer>(customerRepository.findCustomerByPin(id, pin), HttpStatus.OK);
    }

    // Get AppUser By customerId
    @GetMapping("/appUser/customerId/{id}")
    public ResponseEntity<AppUser> getAppUserByCustomerId(@PathVariable Integer id){
        return new ResponseEntity<AppUser>(appUserRepository.findByCustomerId(id), HttpStatus.OK);
    }

    // New User
    // Get AppUser By username
    @GetMapping("/appUser/username/{username}")
    public ResponseEntity<AppUser> getAppUserByUsername(@PathVariable String username){
        return new ResponseEntity<AppUser>(appUserRepository.findByUsername(username), HttpStatus.OK);
    }

    // Reset Password
    // Get AppUser By password
    @GetMapping("/appUser/customerId/{id}/password/{password}")
    public ResponseEntity<AppUser> getAppUserByCustomerIdAndPassword(@PathVariable Integer id, @PathVariable String password){
        AppUser appUser = appUserRepository.findByCustomerId(id);
        if(passwordEncoder.matches(password, appUser.getPassword())){
            return new ResponseEntity<AppUser>(appUserRepository.findByCustomerId(id), HttpStatus.OK);
        }else{
            return new ResponseEntity<AppUser>(appUserRepository.findByPassword(id, password), HttpStatus.OK);
        }
    }

    // Update AppUser's Password by customerId
    @PutMapping("/appUser/customerId/{id}/resetPassword")
    private ResponseEntity<AppUser> resetAppUserPassword(@PathVariable Integer id, @RequestBody AppUser newAppUser){
        Customer customer = customerRepository.findById(id)
                .orElseThrow(() -> new ResourceAccessException("Customer Doesn't Exist"));
        AppUser appUser = appUserRepository.findByCustomerId(id);

        // Get Datetime
        Date date = Calendar.getInstance().getTime();

        // Update App user
        appUser.setPassword(passwordEncoder.encode(newAppUser.getPassword()));
        appUser.setLastUpdatedBy(customer.getName());
        appUser.setLastUpdatedDate(date);
        appUserRepository.save(appUser);

        return ResponseEntity.ok(appUser);
    }

    // Forget Password
    // Get Customer by customerId
    @GetMapping("/customer/{id}")
    public ResponseEntity<Customer> getCustomerByCustomerId(@PathVariable Integer id){
        Customer customer = customerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Customer not exist with id: " + id));
        return ResponseEntity.ok(customer);
    }

    // Update AppUser's Password to Default by customerId
    @PutMapping("/appUser/customerId/{id}/updatePasswordToDefault")
    private ResponseEntity<AppUser> updateAppUserPasswordToDefault(@PathVariable Integer id){
        AppUser appUser = appUserRepository.findByCustomerId(id);

        // Get Datetime
        Date date = Calendar.getInstance().getTime();

        // Update Customer's Password to Default
        appUser.setPassword("DefaultPassword123");
        appUser.setLastUpdatedBy("Admin");
        appUser.setLastUpdatedDate(date);
        appUserRepository.save(appUser);

        return ResponseEntity.ok(appUser);
    }
}