package com.example.internetbankingbackend.controller;

import com.example.internetbankingbackend.exception.ResourceNotFoundException;
import com.example.internetbankingbackend.model.UserAdmin;
import com.example.internetbankingbackend.model.CreditCardApplicant;
import com.example.internetbankingbackend.model.Customer;
import com.example.internetbankingbackend.model.Role;
import com.example.internetbankingbackend.repository.UserAdminRepository;
import com.example.internetbankingbackend.repository.CreditCardApplicantRepository;
import com.example.internetbankingbackend.repository.CustomerRepository;
import com.example.internetbankingbackend.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping
public class ManualUnderwritingController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CreditCardApplicantRepository creditCardApplicantRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserAdminRepository userAdminRepository;

    // Get All Roles Data
    @GetMapping("/roles")
    public List<Role> getAllRoles(){
        return roleRepository.findAll();
    }

    // Get All Admins Data
    @GetMapping("/userAdmins")
    public List<UserAdmin> getAllUserAdmins(){
        return userAdminRepository.findAll();
    }

    // Login
    // Get User Admin Using Username and Password
    @GetMapping("/userAdmin/{username}/{password}")
    public ResponseEntity<UserAdmin> getUserAdminByUsernameAndPassword(@PathVariable String username, @PathVariable String password){
        return new ResponseEntity<UserAdmin>(userAdminRepository.findByUsernameAndPassword(username,password), HttpStatus.OK);
    }

    // Get Role by roleId
    @GetMapping("/role/{id}")
    public ResponseEntity<Role> getRoleByRoleId(@PathVariable Integer id){
        Role role = roleRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Role not exist with id: " + id));
        return ResponseEntity.ok(role);
    }

    // Profile
    // Get Admin by adminId
    @GetMapping("/userAdmin/{id}")
    public ResponseEntity<UserAdmin> getUserAdminById(@PathVariable Integer id){
        UserAdmin userAdmin = userAdminRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User Admin not exist with id: " + id));
        return ResponseEntity.ok(userAdmin);
    }

    // Get User Admin Password By Id and Password
    @GetMapping("/userAdmin/{id}/password/{password}")
    public ResponseEntity<UserAdmin> getUserAdminPassword(@PathVariable Integer id, @PathVariable String password){
        return new ResponseEntity<UserAdmin>(userAdminRepository.findByIdAndPassword(id, password), HttpStatus.OK);
    }

    // Update User Admin Password
    @PutMapping("/userAdmin/{id}/updatePassword/{newPassword}")
    private ResponseEntity<UserAdmin> updateUserAdminPassword(@PathVariable Integer id, @PathVariable String newPassword){
        UserAdmin userAdmin = userAdminRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User Admin not exist with id: " + id));

        userAdmin.setPassword(newPassword);
        userAdminRepository.save(userAdmin);

        return ResponseEntity.ok(userAdmin);
    }

    // Admin
    // Create User Admin
    @PostMapping("/userAdmin/post")
    private UserAdmin createUserAdmin(@RequestBody UserAdmin userAdmin){
        return userAdminRepository.save(userAdmin);
    }

    // Verifikator
    // Get All Applicants Data By Status Not Verified
    @GetMapping(value = "/creditCardApplicants/notVerified")
    public List<Map<String, Object>> getCreditCardApplicantsStatusNotVerified(){
        List<Map<String, Object>> result = new ArrayList<>();
        List<CreditCardApplicant> ccApplicants = creditCardApplicantRepository.findByStatus("Not Verified");
        ccApplicants.forEach(applicant -> {
            if (applicant.getCustomerId() != null) {
                Customer customer = customerRepository.findById(applicant.getCustomerId())
                        .orElseThrow(() -> new ResourceAccessException("Customer Doesn't Exist"));

                Map<String, Object> map = new HashMap<String, Object>();
                map.put("name", customer.getName());
                map.put("city", applicant.getCurrentCity());
                map.put("NPWP", applicant.getNPWP());
                map.put("NIK", applicant.getNIK());
                map.put("phoneNumber", customer.getPhoneNumber());
                map.put("address", applicant.getCurrentAddress());
                map.put("accountNumber", applicant.getAccountSavingNumber());
                map.put("province", applicant.getCurrentProvince());
                map.put("district", applicant.getCurrentKecamatan());
                map.put("ward", applicant.getCurrentKelurahan());
                map.put("RTRW", applicant.getCurrentRTRW());
                map.put("zipCode", applicant.getCurrentZipCode());
                map.put("occupation", applicant.getOccupation());
                map.put("monthlyIncome", applicant.getMonthlyIncome());
                map.put("applicantId", applicant.getApplicantId());
                result.add(map);
            }
        });
        return result;
    }

    // Applicant Verified
    @PutMapping("/creditCardApplicants/{id}/verified")
    private ResponseEntity<CreditCardApplicant> applicantVerified(@PathVariable Integer id){
        CreditCardApplicant applicant = creditCardApplicantRepository.findById(id)
                .orElseThrow(() -> new ResourceAccessException("Applicant Doesn't Exist"));

        applicant.setStatus("Verified");
        creditCardApplicantRepository.save(applicant);

        return ResponseEntity.ok(applicant);
    }

    // Validator
    // Get All Applicants Data By Status Verified
    @GetMapping("/creditCardApplicants/verified")
    public List<CreditCardApplicant> getCreditCardApplicantsStatusVerified(){
        return creditCardApplicantRepository.findByStatus("Verified");
    }

    // Get All Customers Data From List of customerId
    @GetMapping("/creditCardApplicants/customers/{customerIds}")
    public List<Customer> getCustomersData(@PathVariable List<Integer> customerIds){
        List<Customer> customersFromCustomerId = new ArrayList<>();
        for (int i = 0; i < customerIds.size(); i++) {
            Customer customer = customerRepository.findById(customerIds.get(i))
                    .orElseThrow(() -> new ResourceAccessException("Customer Doesn't Exist"));
            customersFromCustomerId.add(customer);
        }
        return customersFromCustomerId;
    }

    // Applicant Approved
    @PutMapping("/creditCardApplicants/{id}/approved")
    private ResponseEntity<CreditCardApplicant> applicantApproved(@PathVariable Integer id){
        CreditCardApplicant applicant = creditCardApplicantRepository.findById(id)
                .orElseThrow(() -> new ResourceAccessException("Applicant Doesn't Exist"));

        applicant.setStatus("Approved");
        creditCardApplicantRepository.save(applicant);

        return ResponseEntity.ok(applicant);
    }

    // Applicant Rejected
    @PutMapping("/creditCardApplicants/{id}/rejected")
    private ResponseEntity<CreditCardApplicant> applicantRejected(@PathVariable Integer id){
        CreditCardApplicant applicant = creditCardApplicantRepository.findById(id)
                .orElseThrow(() -> new ResourceAccessException("Applicant Doesn't Exist"));

        applicant.setStatus("Rejected");
        creditCardApplicantRepository.save(applicant);

        return ResponseEntity.ok(applicant);
    }
}