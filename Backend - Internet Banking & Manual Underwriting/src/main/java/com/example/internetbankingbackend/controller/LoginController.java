package com.example.internetbankingbackend.controller;

import com.example.internetbankingbackend.model.AppUser;
import com.example.internetbankingbackend.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RestController
@RequestMapping(path = "/customer")
public class LoginController {

    @Autowired
    private CustomerService customerService;

    public LoginController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping("/login")
    private ResponseEntity<Map<String, Object>> login(@RequestBody AppUser appUser, HttpServletResponse servletResponse) {
        return customerService.login(appUser, servletResponse);
    }

    @PostMapping("/logout")
    private ResponseEntity<Map<String, Object>> logout(@RequestBody AppUser appUser) {
        return customerService.logout(appUser);
    }

}