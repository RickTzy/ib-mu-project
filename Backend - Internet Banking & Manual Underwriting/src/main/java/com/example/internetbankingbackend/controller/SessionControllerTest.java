package com.example.internetbankingbackend.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.*;
import java.io.IOException;
import java.util.*;

// Contoh Session: https://www.techgeeknext.com/spring-boot/spring-boot-session-management

// Buat session bisa, apusnya masi gagal
@RestController
@RequestMapping(path = "/sessionTest")
public class SessionControllerTest extends HttpServlet {

    private HttpServletRequest requestTest;
    private String sessionIdTest;

    @PostMapping("/login")
    private ResponseEntity<Map<String, Object>> testLogin(HttpServletRequest request, HttpServletResponse responses) throws IOException {
        // Get Session
        List<String> userLoggedIn = (List<String>) request.getSession().getAttribute("SESSION_TEST");
        if(userLoggedIn == null){
            userLoggedIn = new ArrayList<>();
            request.getSession().setAttribute("SESSION_TEST", userLoggedIn);
        }

        // Add new Attribute
        userLoggedIn.add("username");
        request.getSession().setAttribute("SESSION_TEST", userLoggedIn);

        // Test Output
        Map<String, Object> response = new HashMap<>();
        requestTest = request;
        this.sessionIdTest = request.getSession().getId();
        response.put("sessionIdLogin", request.getSession().getId());
        response.put("sessionIdLoginRequestTest", requestTest.getSession().getId());
        response.put("sessionAttributes", request.getSession().getAttribute("SESSION_TEST"));

        Enumeration<String> attributes = request.getSession().getAttributeNames();
        while(attributes.hasMoreElements()){
            String attribute = (String) attributes.nextElement();
            int angka = 1;
            response.put("Attribute ke " + angka + ": ", attribute);
            angka++;
        }

        return ResponseEntity.ok(response);
    }

    @PostMapping("/logout")
    private ResponseEntity<Map<String, Object>> testLogout(HttpServletRequest request){
        // Test Output
        Map<String, Object> response = new HashMap<>();
        response.put("sessionIdLogout", request.getSession().getId());
        response.put("sessionIdLoginRequestTest", requestTest.getSession().getId());
        response.put("sessionIdLoginVariableSessionIdTest", this.sessionIdTest);

        // Delete Session
        request.getSession().invalidate();

        return ResponseEntity.ok(response);
    }
}
