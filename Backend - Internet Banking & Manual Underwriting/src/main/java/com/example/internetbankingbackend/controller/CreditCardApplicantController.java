package com.example.internetbankingbackend.controller;

import com.example.internetbankingbackend.exception.ResourceNotFoundException;
import com.example.internetbankingbackend.jwt.Token;
import com.example.internetbankingbackend.model.Account;
import com.example.internetbankingbackend.model.AppUser;
import com.example.internetbankingbackend.model.CreditCardApplicant;
import com.example.internetbankingbackend.repository.AccountRepository;
import com.example.internetbankingbackend.repository.AppUserRepository;
import com.example.internetbankingbackend.repository.CreditCardApplicantRepository;
import com.example.internetbankingbackend.jwt.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping
public class CreditCardApplicantController {

    @Autowired
    private CreditCardApplicantRepository creditCardApplicantRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private TokenService tokenService;

    // Get All Applicants Data
    @GetMapping("/creditCardApplicants")
    public List<CreditCardApplicant> getAllCCApplicants(){
        return creditCardApplicantRepository.findAll();
    }

    @PostMapping(value = "/postCCApplicant")
    private String saveCCApplicant(@RequestBody CreditCardApplicant creditCardApplicant){
        creditCardApplicantRepository.save(creditCardApplicant);
        return "Saved!";
    }

    @PutMapping(value = "/updateCCApplicant/{id}")
    private String updateCCApplicant(@PathVariable Integer id, @RequestBody CreditCardApplicant creditCardApplicant){
        CreditCardApplicant updatedCCApplicant = creditCardApplicantRepository.findById(id).get();
        updatedCCApplicant.setNIK(creditCardApplicant.getNIK());
        updatedCCApplicant.setNPWP(creditCardApplicant.getNPWP());
        updatedCCApplicant.setPaycheck(creditCardApplicant.getPaycheck());

        updatedCCApplicant.setCurrentAddress(creditCardApplicant.getCurrentAddress());
        updatedCCApplicant.setCurrentRTRW(creditCardApplicant.getCurrentRTRW());
        updatedCCApplicant.setCurrentKelurahan(creditCardApplicant.getCurrentKelurahan());
        updatedCCApplicant.setCurrentKecamatan(creditCardApplicant.getCurrentKecamatan());
        updatedCCApplicant.setCurrentCity(creditCardApplicant.getCurrentCity());
        updatedCCApplicant.setCurrentProvince(creditCardApplicant.getCurrentProvince());
        updatedCCApplicant.setCurrentZipCode(creditCardApplicant.getCurrentZipCode());
        updatedCCApplicant.setOccupation(creditCardApplicant.getOccupation());
        updatedCCApplicant.setAccountSavingType(creditCardApplicant.getAccountSavingType());
        updatedCCApplicant.setAccountSavingNumber(creditCardApplicant.getAccountSavingNumber());
        updatedCCApplicant.setMonthlyIncome(creditCardApplicant.getMonthlyIncome());

        updatedCCApplicant.setCard(creditCardApplicant.getCard());
        updatedCCApplicant.setSignature(creditCardApplicant.getSignature());

        creditCardApplicantRepository.save(updatedCCApplicant);
        return "Updated!";
    }

    @DeleteMapping(value = "/deleteCCApplicant/{id}")
    private String deleteCCApplicant(@PathVariable Integer id){
        CreditCardApplicant deleteCCApplicant = creditCardApplicantRepository.findById(id).get();
        creditCardApplicantRepository.delete(deleteCCApplicant);
        return "Delete CC Form with the id: "+id;
    }

    // Get AppUser By userId
    @GetMapping("/appUser/{id}")
    public ResponseEntity<AppUser> getAppUserById(@PathVariable Integer id){
        AppUser appUser = appUserRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("AppUser not exist with id: " + id));
        return ResponseEntity.ok(appUser);
    }

    // Get Account By accountId
    @GetMapping("/account/{id}")
    public ResponseEntity<Account> getAccountById(@PathVariable Integer id){
        Account account = accountRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Account not exist with id: " + id));
        return ResponseEntity.ok(account);
    }

    // Check Token with Username
    @PostMapping("/verifyToken/{id}")
    public Boolean verifyToken(@PathVariable Integer id, @RequestBody Token token){
        AppUser appUser = appUserRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User not exist with id: " + id));

        if(appUser != null && tokenService.validateToken(token.getToken(), appUser.getUsername())){
            return true;
        }else{
            return false;
        }
    }
}
