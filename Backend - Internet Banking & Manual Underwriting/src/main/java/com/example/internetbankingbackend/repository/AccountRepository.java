package com.example.internetbankingbackend.repository;

import com.example.internetbankingbackend.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Integer> {
    public List<Account> findALlByCustomerId (Integer user_id);
}
