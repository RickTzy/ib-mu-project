package com.example.internetbankingbackend.repository;

import com.example.internetbankingbackend.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
}
