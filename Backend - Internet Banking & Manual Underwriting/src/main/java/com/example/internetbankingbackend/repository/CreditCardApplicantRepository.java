package com.example.internetbankingbackend.repository;

import com.example.internetbankingbackend.model.CreditCardApplicant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CreditCardApplicantRepository extends JpaRepository<CreditCardApplicant, Integer> {
    @Query(value = "SELECT * FROM credit_card_applicant WHERE status =:status", nativeQuery = true)
    public List<CreditCardApplicant> findByStatus(@Param("status") String status);
}
