package com.example.internetbankingbackend.repository;

import com.example.internetbankingbackend.model.UserAdmin;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;

public interface UserAdminRepository extends JpaRepository<UserAdmin, Integer> {
    @Query(value = "SELECT * FROM user_admin WHERE username =:username AND password =:password", nativeQuery = true)
    public UserAdmin findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    @Query(value = "SELECT * FROM user_admin WHERE admin_id =:id AND password =:password", nativeQuery = true)
    public UserAdmin findByIdAndPassword(@Param("id") Integer id, @Param("password") String password);
}
