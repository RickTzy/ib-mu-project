package com.example.internetbankingbackend.repository;

import com.example.internetbankingbackend.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Integer> {
    public AppUser findByUsername(String username);

    @Query(value = "SELECT * FROM app_user WHERE customer_id =:id", nativeQuery = true)
    public AppUser findByCustomerId(@Param("id") Integer id);

    @Query(value = "SELECT * FROM app_user WHERE customer_id =:id AND password =:password", nativeQuery = true)
    public AppUser findByPassword(@Param("id") Integer id,@Param("password") String password);

    public Optional<AppUser> findOneByUsernameAndStatus(String username, String status);

}
