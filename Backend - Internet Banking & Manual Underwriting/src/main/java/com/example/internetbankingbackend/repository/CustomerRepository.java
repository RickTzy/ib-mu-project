package com.example.internetbankingbackend.repository;

import com.example.internetbankingbackend.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    public Customer findByPan(String pan);

    @Query(value = "SELECT * FROM customer WHERE customer_id = :id AND pin = :pin", nativeQuery = true)
    public Customer findCustomerByPin(@Param("id") Integer id, @Param("pin") Integer pin);
}
