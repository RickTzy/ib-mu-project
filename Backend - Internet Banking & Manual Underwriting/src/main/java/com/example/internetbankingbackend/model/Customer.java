package com.example.internetbankingbackend.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer customerId;

    private String cifCode;
    private String pan;
    private Integer pin;
    private String title;
    private String name;
    private String gender;
    private String maritalStatus;
    private String address;
    private String RTRW;
    private String kelurahan;
    private String kecamatan;
    private String city;
    private String province;
    private Integer zipCode;
    private String birthPlace;
    private String birthDate;
    private String status;
    private String phoneNumber;
    private Date createdDate;
    private String createdBy;
    private Date lastUpdatedDate;
    private String lastUpdatedBy;
}
