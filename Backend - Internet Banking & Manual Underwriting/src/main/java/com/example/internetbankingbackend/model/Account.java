package com.example.internetbankingbackend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer accountId;

    private Integer customerId;
    private String accountNumber;
    private String accountType;
    private Double balance;
    private String status;
    private Date createdDate;
    private String createdBy;
    private Date lastUpdatedDate;
    private String LastUpdatedBy;

    public String toString() {
        return "{" +
                "\"accountId\": " + accountId + "," +
                "\"customerId\": " + customerId + "," +
                "\"accountNumber\": " + accountNumber + "," +
                "\"accountType\": " + accountType + "," +
                "\"status\": " + status+ "," +
                "\"createdDate\": " + createdDate + "," +
                "\"createdBy\": " + createdBy + "," +
                "\"lastUpdatedDate\": " + lastUpdatedDate+ "," +
                "\"lastUpdateBy\": " + LastUpdatedBy +
                "}";
    }
}
