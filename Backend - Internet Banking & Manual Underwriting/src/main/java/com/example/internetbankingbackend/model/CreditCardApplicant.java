package com.example.internetbankingbackend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "creditCardApplicant")
public class CreditCardApplicant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer applicantId;

    private Integer customerId;
    private Integer userId;

    //  CC Form 1
    @Column
    private String NIK;

    @Column
    private String NPWP;

    @Lob
    private String paycheck;

    //  CC Form 2
    @Column
    private String currentAddress;

    @Column
    private String currentRTRW;

    @Column
    private String currentKelurahan;

    @Column
    private String currentKecamatan;

    @Column
    private String currentCity;

    @Column
    private String currentProvince;

    @Column
    private String currentZipCode;

    @Column
    private String occupation;

    @Column
    private String accountSavingType;

    @Column
    private String accountSavingNumber;

    @Column
    private Double monthlyIncome;

    //  CC Form 3
    @Column
    private String card;

    @Lob
    private String signature;

    @Column
    private String status;
}
