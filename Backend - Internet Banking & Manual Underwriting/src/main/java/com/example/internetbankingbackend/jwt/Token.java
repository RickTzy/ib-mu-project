package com.example.internetbankingbackend.jwt;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Token {
    private String token;

}
