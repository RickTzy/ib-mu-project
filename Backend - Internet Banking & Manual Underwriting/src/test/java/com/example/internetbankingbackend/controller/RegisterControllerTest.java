package com.example.internetbankingbackend.controller;

import com.example.internetbankingbackend.model.Account;
import com.example.internetbankingbackend.model.AppUser;
import com.example.internetbankingbackend.model.Customer;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.*;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class RegisterControllerTest {

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Order(7)
    public void getAllCustomers() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/customers")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].customerId").isNotEmpty());
    }

    @Test
    @Order(8)
    public void getAllAccounts() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/accounts")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].accountId").isNotEmpty());
    }

    @Test
    @Order(9)
    public void getAllAppUsers() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/appUsers")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].userId").isNotEmpty());
    }

    @Test
    @Order(1)
    public void createCustomer() throws Exception{
        Customer customer = new Customer(null, "C12345678", "1234567891", 291221, "Mr", "Budi Setiawan", "Male", "Kawin", "Jl. My House No. 1", "001/002", "Palmerah", "Kemanggisan", "Jakarta Barat", "DKI Jakarta", 10120, "Jakarta", "1998-09-23", "Active", "081312345678", getDateTimeNow(), "Admin", getDateTimeNow(), "Admin");

        mockMvc.perform(MockMvcRequestBuilders
                .post("/customer/post")
                .content(asJsonString(customer))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.customerId").isNotEmpty());
    }

    @Test
    @Order(2)
    public void createCustomers() throws Exception{
        Customer customer1 = new Customer(null, "C01740380", "2000633837", 389479, "Mr", "Jarwa Winarno", "Male", "Kawin", "Dk. Babadan No. 150", "001/012", "Pariaman", "Pariaman", "Surabaya", "Jawa Timur", 97943, "Surabaya", "1993-01-09", "Active", "08196332445", getDateTimeNow(), "Admin", getDateTimeNow(), "Admin");
        Customer customer2 = new Customer(null, "C2663484J", "8385167729", 739961, "Mrs", "Hana Hastuti", "Female", "Kawin", "Jln. Ujung No. 535", "054/055", "Binjai", "Binjai", "DIY", "DIY", 57651, "DIY", "1996-09-23", "Active", "081780301198", getDateTimeNow(), "Admin", getDateTimeNow(), "Admin");
        Customer customer3 = new Customer(null, "C13488069", "2085473250", 580890, "Mr", "Cager Ardianto", "Male", "Belum Kawin", "Jr. Bakau No. 963", "009/008", "Bandang", "Bandang", "Medan", "Sumatera Utara", 39016, "Medan", "1999-09-03", "Active", "0878092996", getDateTimeNow(), "Admin", getDateTimeNow(), "Admin");

        List<Customer> customers = new ArrayList<>(Arrays.asList(customer1, customer2, customer3));
        mockMvc.perform(MockMvcRequestBuilders
                .post("/customers/post")
                .content(asJsonString(customers))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].customerId").isNotEmpty());
    }

    @Test
    @Order(3)
    public void createAccount() throws Exception{
        Account account = new Account(null, 1, "1234567899", "Simas Gold", (double)25890000, "Active", getDateTimeNow(), "Admin", getDateTimeNow(), "Admin");

        mockMvc.perform(MockMvcRequestBuilders
                .post("/account/post")
                .content(asJsonString(account))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.accountId").isNotEmpty());
    }

    @Test
    @Order(4)
    public void createAccounts() throws Exception{
        Account account1 = new Account(null, 2, "2000633837", "Simas Gold", (double)63240000, "Active", getDateTimeNow(), "Admin", getDateTimeNow(), "Admin");
        Account account2 = new Account(null, 3, "8385167729", "Simas Digi", (double)45940500, "Active", getDateTimeNow(), "Admin", getDateTimeNow(), "Admin");
        Account account3 = new Account(null, 4, "2085473250", "Simas Digi", (double)11300500, "Active", getDateTimeNow(), "Admin", getDateTimeNow(), "Admin");

        List<Account> accounts = new ArrayList<>(Arrays.asList(account1, account2, account3));
        mockMvc.perform(MockMvcRequestBuilders
                .post("/accounts/post")
                .content(asJsonString(accounts))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].accountId").isNotEmpty());
    }

    @Test
    @Order(5)
    public void createAppUser() throws Exception{
        AppUser appUser = new AppUser(null, 2, "Alpha", "alpha123", 0, false, "alpha@tester.com", "Active", getDateTimeNow(), "Admin", getDateTimeNow(), "Admin");

        mockMvc.perform(MockMvcRequestBuilders
                .post("/appUser/post")
                .content(asJsonString(appUser))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.userId").isNotEmpty());
    }

    @Test
    @Order(6)
    public void createAppUsers() throws Exception{
        AppUser appUser1 = new AppUser(null, 3, "Beta", passwordEncoder.encode("beta123"), 0, false, "beta@tester.com", "Active", getDateTimeNow(), "Admin", getDateTimeNow(), "Admin");
        AppUser appUser2 = new AppUser(null, 4, "Omega", passwordEncoder.encode("omega123"), 0, false, "omega@tester.com", "Active", getDateTimeNow(), "Admin", getDateTimeNow(), "Admin");

        List<AppUser> appUsers = new ArrayList<>(Arrays.asList(appUser1, appUser2));
        mockMvc.perform(MockMvcRequestBuilders
                .post("/appUsers/post")
                .content(asJsonString(appUsers))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].userId").isNotEmpty());
    }

    @Test
    @Order(10)
    public void getCustomerByPan() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/customer/pan/{pan}", 1234567891)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.cifCode").value("C12345678"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Budi Setiawan"));
    }

    @Test
    @Order(11)
    public void getCustomerByCustomerIdAndPin() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/customer/{id}/pin/{pin}", 1, 291221)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.pan").value("1234567891"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.phoneNumber").value("081312345678"));
    }

    @Test
    @Order(12)
    public void getAppUserByCustomerId() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/appUser/customerId/{id}",2)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("Alpha"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("alpha@tester.com"));
    }

    @Test
    @Order(13)
    public void getAppUserByUsername() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/appUser/username/{username}","Beta")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.customerId").value(3))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("beta@tester.com"));
    }

    @Test
    @Order(14)
    public void getAppUserByCustomerIdAndPassword() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/appUser/customerId/{id}/password/{password}", 4, "omega123")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("Omega"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("omega@tester.com"));
    }

    @Test
    @Order(17)
    public void resetAppUserPassword() throws Exception{
        AppUser newAppUser = new AppUser();
        newAppUser.setPassword("newpassword123");

        mockMvc.perform( MockMvcRequestBuilders
                .put("/appUser/customerId/{id}/resetPassword", 4)
                .content(asJsonString(newAppUser))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.customerId").value(4))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastUpdatedBy").value("Cager Ardianto"));
    }

    @Test
    @Order(16)
    public void getCustomerByCustomerId() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/customer/{id}", 2)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.cifCode").value("C01740380"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Jarwa Winarno"));
    }

    @Test
    @Order(15)
    public void updateAppUserPasswordToDefault() throws Exception{
        mockMvc.perform( MockMvcRequestBuilders
                .put("/appUser/customerId/{id}/updatePasswordToDefault", 4)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("omega@tester.com"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.password").value("DefaultPassword123"));
    }

    private Date getDateTimeNow(){
        return Calendar.getInstance().getTime();
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}