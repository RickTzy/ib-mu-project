package com.example.internetbankingbackend.controller;

import com.example.internetbankingbackend.model.CreditCardApplicant;
import com.example.internetbankingbackend.repository.CreditCardApplicantRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CreditCardApplicantControllerTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private CreditCardApplicantRepository creditCardApplicantRepository;

    @Test
//    @Transactional
    public void insertDatabaseTestController() {
//        assertThat(this.restTemplate.postForObject("http://localhost:" + port + "/api/postcc"));
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        String url = "http://localhost:" + port + "/postCCApplicant";
        CreditCardApplicant creditCardApplicant = new CreditCardApplicant();

        creditCardApplicant.setNIK("123456789134567");
        creditCardApplicant.setNPWP("123451234512345");
        HttpEntity<CreditCardApplicant> httpEntity = new HttpEntity<>(creditCardApplicant, header);

        String response = restTemplate.postForObject(url, httpEntity, String.class);
        System.out.println(response);
        assertEquals("Saved!", response);

        long totalRow = creditCardApplicantRepository.count();
        System.out.println(totalRow);

        CreditCardApplicant creditCardApplicant1 = creditCardApplicantRepository.findById((int) totalRow).get();

        assertEquals(creditCardApplicant.getNIK(), creditCardApplicant1.getNIK());
        assertEquals(creditCardApplicant.getNPWP(), creditCardApplicant1.getNPWP());

    }
}