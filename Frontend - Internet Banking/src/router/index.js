import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/HomePages.vue')
  },
  { 
    path: "/product",
    name: "productoffer",
    component: () => import('../views/ProductOffer.vue')
  },
  { 
    path: "/aboutUs",
    name: "aboutus",
    component: () => import('../views/AboutUs.vue')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/Register.vue')
  },
  {
    path: '/newUser',
    name: 'newUser',
    component: () => import('../views/NewUser.vue')
  },
  {
    path: '/resetPassword',
    name: 'resetPassword',
    component: () => import('../views/ResetPassword.vue')
  },
  {
    path: '/forgotPassword',
    name: 'forgotPassword',
    component: () => import('../views/ForgotPassword.vue')
  },
  {
    path: '/applyCC',
    name: 'applyCC',
    component: () => import('../views/ApplyCreditCard.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
